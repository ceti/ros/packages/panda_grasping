# Panda Grasping Package

Allows to pick and place objects. The current maximum height, where the panda can grip (from above) ist 50cm. This package uses the old pick-and-place pipeline of MoveIt.

## How to install

* cd into the src-folder of your workspace
* clone this package
* catkin build & source the workspace
* that's it

## How to use 

* u can start the configurable pick and place with: ```roslaunch panda_grasping simulation.launch```
    * to pick or place open a second console
    * to pick type for example: ```rosservice call panda_grasping/PickObjectService '{pose: {position: {x: 0.5, y: 0.0, z: 0.2}, orientation: {x: 0.0, y: 0.0, z: 0.0, w: 1.0}}, dimensions: {x: 0.02, y: 0.02, z: 0.2}}'```
    * to place type for example ```rosservice call panda_grasping/PlaceObjectService '{pose: {position: {x: 0.0, y: 0.5, z: 0.2}, orientation: {x: 0.0, y: 0.0, z: 0.0, w: 1.0}}, dimensions: {x: 0.02, y: 0.02, z: 0.2}}'```
    * inside of the message u can update orientation (currenly only z is supported), position and size of the object to pick or place

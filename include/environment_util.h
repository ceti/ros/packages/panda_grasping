//
// Created by sebastian on 19.05.20.
//

#ifndef PANDA_GRASPING_ENVIRONMENT_UTIL_H
#define PANDA_GRASPING_ENVIRONMENT_UTIL_H

#include <ros/ros.h>

#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>

/**
 * Util for the construction of the scene.
 */
class EnvironmentUtil {

public:

    /**
     * Adds a table plate to the planning scene.
     * @param collision_objects current collision objects in the planning scene
     * @param x_dimension width of the table
     * @param y_dimension length of the table
     */
    void constructPlate(std::vector<moveit_msgs::CollisionObject> &collision_objects, double x_dimension,
                        double y_dimension);

    /**
    * Adds a support surface to the planning scene (with ID "bin").
    * @param collision_objects current collision objects in the planning scene
    * @param id id of the support surface to create
    * @param object_x_dimension x dimension object to be picked
    * @param object_y_dimension y dimension object to be picked
    * @param object_z_dimension z dimension object to be picked
     *@param object_to_pick_pose pose of the object to be picked
    */
    void constructSupport(std::vector<moveit_msgs::CollisionObject> &collision_objects, std::string id,
                          double object_x_dimension, double object_y_dimension, double object_z_dimension,
                          geometry_msgs::Pose &object_to_pick_pose);

    /**
     * Adds a object to be picked/placed to the planning scene.
     * @param collision_objects current collision objects in the planning scene
     * @param id id of the object to create
     * @param pose pose of the object to be picked
     * @param dimensions dimensions of the object to be picked
     */
    moveit_msgs::CollisionObject
    constructObjectToPick(std::vector<moveit_msgs::CollisionObject> &collision_objects, std::string id,
                          geometry_msgs::Pose &pose, geometry_msgs::Vector3 &dimensions);
};

#endif //PANDA_GRASPING_ENVIRONMENT_UTIL_H

//
// Created by jm on 03.12.20.
//

#ifndef PUBLIC_FRANKA_GRIPPER_UTIL_H
#define PUBLIC_FRANKA_GRIPPER_UTIL_H


class FrankaGripperUtil {

public:
    /**
    * resets the real robot, to allow correct future movements and grasps
    */
    void resetGripperForNextAction();

};


#endif //PUBLIC_FRANKA_GRIPPER_UTIL_H

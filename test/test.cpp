//
// Created by Johannes Mey on 22.11.20.
//

#include "ros/ros.h"

#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2/LinearMath/Quaternion.h"
#include "tf2/LinearMath/Vector3.h"

#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>

#include "panda_grasping/PickObject.h"
#include "panda_grasping/PlaceObject.h"

#include <gtest/gtest.h>

class Grasping : public ::testing::Test {
protected:

    void SetUp() override {
        ros::NodeHandle n("panda_grasping");
        pickClient = n.serviceClient<panda_grasping::PickObject>("PickObjectService");
        placeClient = n.serviceClient<panda_grasping::PlaceObject>("PlaceObjectService");

        // clean up scene
        CleanupScene();
    }


    static void CleanupScene() {
        moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

        for (auto p : planning_scene_interface.getObjects()) {
            p.second.operation = moveit_msgs::CollisionObject::REMOVE;
            planning_scene_interface.applyCollisionObject(p.second);
        }
        ASSERT_EQ(planning_scene_interface.getObjects().size(), 0);
        ASSERT_EQ(planning_scene_interface.getAttachedObjects().size(), 0);
    }

    // void TearDown() override {}

    ros::ServiceClient pickClient;
    ros::ServiceClient placeClient;

    std::vector<tf2::Vector3> objects{
            tf2::Vector3(0.01, 0.1, 0.1),
            tf2::Vector3(0.05, 0.1, 0.1),
            tf2::Vector3(0.005, 0.1, 0.1)
    };

    std::vector<tf2::Vector3> points{
            tf2::Vector3(0.5, 0.3, 0.2),
            tf2::Vector3(-0.5, -0.3, 0.3),
            tf2::Vector3(0.5, 0.3, 0.1),
            tf2::Vector3(0.4, 0.5, 0.4),

    };

    tf2::Vector3 nullLocation{-0.4, 0.5, 0};

    std::vector<double> angles{
            M_PI_2,
            1,
            -M_PI_4
    };

    void pickAndPlace(tf2::Vector3 size, tf2::Vector3 from, double fromAngle, tf2::Vector3 to, double toAngle) {
        // wait a little while
        ros::Duration(0.5).sleep();
        ROS_INFO_STREAM("starting test");
        tf2::Quaternion orientation;
        {
            orientation.setRPY(0, 0, fromAngle);

            panda_grasping::PickObject pickObjectSrv;
            pickObjectSrv.request.dimensions = tf2::toMsg(size);
            tf2::toMsg(from, pickObjectSrv.request.pose.position);
            pickObjectSrv.request.pose.orientation = tf2::toMsg(orientation);

            ROS_INFO_STREAM("starting to pick");

            ASSERT_TRUE(pickClient.exists());
            ASSERT_TRUE(pickClient.call(pickObjectSrv));
        }
        {
            orientation.setRPY(0, 0, toAngle);

            panda_grasping::PlaceObject placeObjectSrv;
            placeObjectSrv.request.dimensions = tf2::toMsg(size);
            tf2::toMsg(to, placeObjectSrv.request.pose.position);
            placeObjectSrv.request.pose.orientation = tf2::toMsg(orientation);

            ROS_INFO_STREAM("starting to place");

            ASSERT_TRUE(placeClient.exists());
            ASSERT_TRUE(placeClient.call(placeObjectSrv));


            ROS_INFO_STREAM("pick and place finished");
        }
    }
};


TEST_F(Grasping, Grasping_WithoutRotation_Object_0_FromTo_01) {
    auto object = objects[0];
    pickAndPlace(object, points[0], 0, points[1], 0);
}

TEST_F(Grasping, Grasping_WithoutRotation_Object_0_FromTo_Null1) {
    auto object = objects[0];
    pickAndPlace(object, nullLocation, 0, points[1], 0);
}

TEST_F(Grasping, Grasping_WithoutRotation_Object_1_FromTo_01) {
    auto object = objects[1];
    pickAndPlace(object, points[0], 0, points[1], 0);
}

TEST_F(Grasping, Grasping_WithoutRotation_Object_2_FromTo_01) {
    auto object = objects[2];
    pickAndPlace(object, points[0], 0, points[1], 0);
}

TEST_F(Grasping, Grasping_WithoutRotation_Object_0_FromTo_1Null) {
    auto object = objects[0];
    pickAndPlace(object, points[1], 0, nullLocation, 0);
}

TEST_F(Grasping, Grasping_WithoutRotation_Object_0_FromTo_01_23) {
    auto object = objects[0];
    pickAndPlace(object, points[0], 0, points[1], 0);
    CleanupScene();
    pickAndPlace(object, points[2], 0, points[3], 0);
}

TEST_F(Grasping, Grasping_WithoutRotation_Object_0_FromTo_01_12) {
    auto object = objects[0];
    pickAndPlace(object, points[0], 0, points[1], 0);
    CleanupScene();
    pickAndPlace(object, points[1], 0, points[2], 0);
}

TEST_F(Grasping, GraspingWithRotation_Object_0_Rotations_00) {
    auto object = objects[0];
    auto fromAngle = angles[0];
    auto toAngle = angles[0];

    pickAndPlace(object, points[0], fromAngle, points[1], toAngle);
}

TEST_F(Grasping, GraspingWithRotation_Object_0_Rotations_01) {
    auto object = objects[0];
    auto fromAngle = angles[0];
    auto toAngle = angles[1];

    pickAndPlace(object, points[0], fromAngle, points[1], toAngle);
}

TEST_F(Grasping, GraspingWithRotation_Object_0_Rotations_12) {
    auto object = objects[0];
    auto fromAngle = angles[1];
    auto toAngle = angles[2];

    pickAndPlace(object, points[0], fromAngle, points[1], toAngle);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "grasping_test");
    ros::NodeHandle n("panda_grasping");

    ROS_INFO_NAMED("constraint_planner", ">>>>>>>>>>>>>>>> WAITING FOR ROBOT INIT <<<<<<<<<<<<<<<");
    ros::Duration(3.0).sleep();
    ROS_INFO_NAMED("constraint_planner", ">>>>>>>>>>>>>>>>> STARTING TEST AFTER INIT <<<<<<<<<<<<<<<<");


    return RUN_ALL_TESTS();
}

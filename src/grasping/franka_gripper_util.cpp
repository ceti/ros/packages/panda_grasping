//
// Created by jm on 03.12.20.
//

#include "franka_gripper_util.h"

// Actionlib
#include <actionlib/client/simple_action_client.h>

// Franka
#include <franka_gripper/MoveAction.h>

void FrankaGripperUtil::resetGripperForNextAction() {

    actionlib::SimpleActionClient<franka_gripper::MoveAction> ac("franka_gripper/move", true);

    ROS_INFO("Waiting for reset action server to start.");
    ac.waitForServer();

    ROS_INFO("Reset action server started, sending goal.");
    franka_gripper::MoveGoal goal;
    goal.speed = 0.08;
    goal.width = 0.0;
    ac.sendGoal(goal);

    //wait for the action to return
    bool finished_before_timeout = ac.waitForResult(ros::Duration(10.0));

    if (finished_before_timeout) {
        actionlib::SimpleClientGoalState state = ac.getState();
        ROS_INFO("Reset action finished: %s", state.toString().c_str());
    } else {
        ROS_INFO("Reset action did not finish before the time out.");
    }
}
